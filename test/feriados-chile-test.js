const Helper = require("hubot-test-helper");
const moxios = require("moxios");
const { expect } = require("chai");

const helper = new Helper("../src/feriados-chile.js");
const feriadosFixture = require("./feriados-fixture.json");

describe("feriados-chile", () => {
  let room;

  beforeEach(() => {
    moxios.install();
    room = helper.createRoom({ httpd: false });
  });

  afterEach(() => {
    moxios.uninstall();
    room.destroy();
  });

  it("responds to get next feriado", () =>
    room.user
      .say("aline", "@hubot feriado")
      .then(() =>
        moxios.requests.mostRecent().respondWith({
          status: 200,
          response: feriadosFixture
        })
      )
      .then(() =>
        expect(room.messages[1]).to.eql([
          "hubot",
          "@aline holar, el siguiente feriado será el 20-03-2050"
        ])
      ));

  it("responds an error when holiday not found", () =>
    room.user
      .say("aline", "@hubot feriado")
      .then(() =>
        moxios.requests.mostRecent().respondWith({
          status: 200,
          response: []
        })
      )
      .then(() =>
        expect(room.messages[1]).to.eql([
          "hubot",
          "@aline Error: Holiday not found"
        ])
      ));

  it("responds to get a list of next feriados", () =>
    room.user
      .say("aline", "@hubot feriados")
      .then(() =>
        moxios.requests.mostRecent().respondWith({
          status: 200,
          response: feriadosFixture
        })
      )
      .then(() =>
        expect(room.messages[1]).to.eql([
          "hubot",
          "@aline holar, los siguientes feriados son: 20-03-2050, 21-03-2050, 22-03-2050"
        ])
      ));
});
