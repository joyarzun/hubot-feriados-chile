# hubot-feriados-chile

A script for consulting holidays in Chile

See [`src/feriados-chile.coffee`](src/feriados-chile.coffee) for full documentation.

## Installation

In hubot project repo, run:

`npm install hubot-feriados-chile --save`

Then add **hubot-feriados-chile** to your `external-scripts.json`:

```json
["hubot-feriados-chile"]
```

## Sample Interaction

```
user1>> hubot feriado
hubot>> holar, el siguiente feriado será el 30-03-2018
```

## NPM Module

https://www.npmjs.com/package/hubot-feriados-chile
