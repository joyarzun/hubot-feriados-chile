// Description
//   A script for consulting holidays in Chile
//
// Configuration:
//   LIST_OF_ENV_VARS_TO_SET
//
// Commands:
//   hubot feriado - get the next holiday
//   hubot feriados - get a list of next holiday
//
// Notes:
//   <optional notes required for the script>
//
// Author:
//   Jonnatan Oyarzun <jonsxaero@gmail.com>

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"; //certificate api has expired
const { get } = require("axios");

module.exports = robot => {
  robot.respond(/feriado$/, res => {
    get("https://apis.modernizacion.cl/fl/feriados/2018")
      .then(({ data }) =>
        res.reply(
          `holar, el siguiente feriado será el ${toLocale(
            getNextHoliday(data)
          )}`
        )
      )
      .catch(err => res.reply(err.toString()));
  });

  robot.respond(/feriados$/, res => {
    get("https://apis.modernizacion.cl/fl/feriados/2018")
      .then(({ data }) =>
        res.reply(
          `holar, los siguientes feriados son: ${getListNexttHolidays(data)
            .map(toLocale)
            .join(", ")}`
        )
      )
      .catch(err => res.reply(err.toString()));
  });
};

const getNextHoliday = (
  data,
  todayTS = new Date().setHours(0, 0, 0, 0),
  i = 0
) => {
  if (i == data.length) throw new Error("Holiday not found");
  const comparisonDateTS = new Date(`${data[i].fecha}GMT-0400`).getTime();
  if (comparisonDateTS >= todayTS) return comparisonDateTS;

  return getNextHoliday(data, todayTS, i + 1);
};

const getListNexttHolidays = data => {
  const v1 = getNextHoliday(data);
  const v2 = getNextHoliday(data, v1 + 1);
  const v3 = getNextHoliday(data, v2 + 1);
  return [v1, v2, v3];
};

const toLocale = time => new Date(time).toLocaleDateString("es-CL");
